# -*- coding: utf-8 -*-

import os
import sys
import time
import itertools


def datetime_to_ts(dt):
    return int(time.mktime(dt.timetuple()))


def cp(src, dst):
    cmd = 'cp -r {SRC} {DST}'.format(SRC=src, DST=dst)
    print >> sys.stderr, cmd
    os.system(cmd)


def rm(target):
    cmd = 'rm -rf {TARGET}'.format(TARGET=target)
    print >> sys.stderr, cmd
    os.system(cmd)


def keys_finder(dct, item):
    return filter(lambda key: dct[key] == item, dct)


def merge_lists(items):
    if not isinstance(items[0], list):
        return items
    return list(itertools.chain(*items))