# -*- coding: utf-8 -*-

from selenium import webdriver


class ProfilesManager:
    class FirefoxProfile(webdriver.FirefoxProfile):
        def selebot_set_proxy(self, proxy=None, fake=False):  # proxy in format HOST:PORT or None
            if proxy is None or fake:
                self.set_preference("network.proxy.type", 0)
            else:
                host, port = proxy.rsplit(":", 1)
                port = int(port)
                self.set_preference("network.proxy.type", 1)
                self.set_preference("network.proxy.http", host)
                self.set_preference("network.proxy.http_port", port)
                self.set_preference("network.proxy.no_proxies_on", "")
                self.set_preference("network.proxy.share_proxy_settings", True)
            self.update_preferences()
            self.selebot_proxy = proxy

        def selebot_set_useragent(self, useragent='Mozilla/5.0'):
            self.set_preference("general.useragent.override", useragent)
            self.update_preferences()
            self.selebot_useragent = useragent