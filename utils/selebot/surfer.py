# -*- coding: utf-8 -*-

import os
import time
import socket
import signal
import tempfile
from urllib2 import URLError
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC

from utils.text.simple import random_string
from profiles_manager import ProfilesManager
from utils.anticaptcha import recognize_captcha_url

SELEBOT_TIMEOUT = 90


class Surfer(object):
    def __init__(self, referer=None, proxy=None, useragent=None, profile=None, fake=False, folder=123):
        self._referer = referer
        self._useragent = useragent
        self._proxy = proxy
        self._profile = profile or ProfilesManager.FirefoxProfile()
        self._profile.set_preference('browser.download.folderList', 2)
        self._profile.set_preference("browser.download.manager.showWhenStarting", False)
        self._profile.set_preference("browser.download.dir", folder)
        self._profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/zip")
        self._proxy = proxy

        if useragent is not None:
            self._profile.selebot_set_useragent(useragent)

        if proxy is not None:
            self._profile.selebot_set_proxy(proxy, fake)

    def __del__(self):
        self.quit_browser()

    def get_region_screenshot(self, x1, y1, x2, y2):
        fn = os.path.join(tempfile.gettempdir(), random_string() + '.png')
        self._browser.get_screenshot_as_file(fn)
        from PIL import Image
        img = Image.open(fn)
        os.remove(fn)
        box = x1, y1, x2, y2
        img = img.crop(box)
        return img

    def get_element_geometry(self, element):
        location = element.location_once_scrolled_into_view
        x = location['x']
        y = location['y']
        size = element.size
        width = size['width']
        height = size['height']
        return x, y, x + width, y + height

    def open_browser(self, start_page=None, timeout=SELEBOT_TIMEOUT):
        self._browser = webdriver.Firefox(firefox_profile=self._profile, timeout=timeout)

        if start_page is None:
            start_page = self._referer

        if start_page == "-":
            return True
        else:
            return self.get(start_page, timeout)

    def quit_browser(self):
        if not hasattr(self, '_browser'):
            return
        attempts = 10
        for attempt in xrange(attempts):
            try:
                self._browser.quit()
                return
            except URLError:
                pass
        pid = self._browser.binary.process.pid
        try:
            os.kill(pid, signal.SIGKILL)
        except OSError:
            pass

    def get(self, url, timeout=SELEBOT_TIMEOUT):
        tm = socket.getdefaulttimeout()
        socket.setdefaulttimeout(timeout)
        status = True
        try:
            self._browser.get(url)
        except socket.timeout:
            try:
                ps = self._browser.page_source
            except socket.timeout:
                self._browser.quit()
                status = False
        socket.setdefaulttimeout(tm)
        return status

    def wait_for_xpath(self, xpath, timeout=SELEBOT_TIMEOUT, timestep=1):
        t = time.time()
        while time.time() - t < timeout:
            try:
                self._browser.find_element_by_xpath(xpath)
                return
            except NoSuchElementException:
                time.sleep(timestep)
        self._browser.find_element_by_xpath(xpath)

    def wait_for_css(self, css, timeout=SELEBOT_TIMEOUT):
        WebDriverWait(self._browser, timeout).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, css))
        )

    def wait_for(self, cond, msg='', timeout=SELEBOT_TIMEOUT):
        t = time.time()
        while time.time() - t < timeout:
            if cond(): break
            time.sleep(0.1)
        if not cond():
            raise Exception(msg)

    def get_browser(self):
        return self._browser

    def solve_captcha(self, img_selector):
        img = self.get_browser().find_element_by_css_selector(img_selector)
        src = img.get_attribute('src')
        return recognize_captcha_url(src)
