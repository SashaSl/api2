# -*- coding: utf-8 -*-

import os
import time
from pyvirtualdisplay.display import Display
from selenium.common.exceptions import NoSuchElementException

import cfg.common


def wait_element_displayed(selector, browser, limit=15):
    timeout_start = time.time()
    while time.time() < timeout_start + limit:
        if isinstance(selector, list):
            for sel in selector:
                try:
                    element = browser.find_element_by_css_selector(sel)
                    if element.is_displayed():
                        return
                except NoSuchElementException:
                    pass
            time.sleep(0.2)
        else:
            try:
                element = browser.find_element_by_css_selector(selector)
                if element.is_displayed():
                    return
            except NoSuchElementException:
                    pass
            time.sleep(0.2)
    raise Exception("waited too long")


class XVFB_handler():
    def __init__(self, visible=cfg.common.xvfb_visible, width=1200, height=600, timeout=30):
        if visible:
            os.environ['DISPLAY'] = ':0'
        self.display = Display(visible=visible, size=(width, height))
        self.display.start()
        time.sleep(timeout)

    def __del__(self):
        self.display.stop()