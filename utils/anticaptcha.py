# -*- coding: utf-8 -*-

import os
import time
import urllib
import httplib

import cfg.common
from utils.text.simple import random_string

ANTI_CAPTCHA_KEY = cfg.common.anti_captcha_key
ANTI_CAPTCHA_TIMEOUT = cfg.common.anti_captcha_timeout
ANTI_CAPTCHA_API_URL = cfg.common.anti_captcha_api_url
ANTI_CAPTCHA_BOUNDARY = cfg.common.anti_captcha_boundary
ANTI_CAPTCHA_DOMAIN = cfg.common.anti_captcha_domain


def recognize_captcha_url(captcha_url, is_russian=False):
    TMP_DIR = "/tmp"
    if not os.path.exists(TMP_DIR):
        os.makedirs(TMP_DIR)
    captcha_image = TMP_DIR + "/" + random_string(16) + ".jpeg"
    urllib.urlretrieve(captcha_url, captcha_image)
    captcha_text = recognize_captcha(captcha_image, is_russian)
    os.remove(captcha_image)
    return captcha_text


def recognize_captcha(captcha_image, is_russian=False):
    status, captcha_text = _recognize_captcha(captcha_image, is_russian)
    if status != "OK":
        os.remove(captcha_image)

        class CaptchaFailed(Exception):
            pass

        raise CaptchaFailed('Captcha failed!')
    return captcha_text


def _recognize_captcha(captcha_image, is_russian=False):
    key = ANTI_CAPTCHA_KEY
    cap_id = __send_captcha_image(key, captcha_image, is_russian)
    time.sleep(ANTI_CAPTCHA_TIMEOUT)
    res_url = ANTI_CAPTCHA_API_URL
    res_url += "?" + urllib.urlencode({'key': key, 'action': 'get', 'id': cap_id})
    while 1:
        res = urllib.urlopen(res_url).read()
        if res == "CAPCHA_NOT_READY":
            time.sleep(1)
            continue
        break

    res = res.split('|')
    if len(res) == 2:
        return tuple(res)
    else:
        return 'ERROR', res[0]


def __send_captcha_image(key, captcha_image, is_russian=False):
    data = open(captcha_image, 'rb').read()
    boundary = ANTI_CAPTCHA_BOUNDARY
    body = '''--%s
Content-Disposition: form-data; name="method"

post
--%s
Content-Disposition: form-data; name="key"

%s
--%s
Content-Disposition: form-data; name="is_russian"

%s
--%s
Content-Disposition: form-data; name="file"; filename="capcha.jpg"
Content-Type: image/pjpeg

%s
--%s--

''' % (boundary, boundary, key, boundary, str(int(is_russian)), boundary, data, boundary)

    headers = {'Content-type': 'multipart/form-data; boundary=%s' % boundary}
    h = httplib.HTTPConnection(ANTI_CAPTCHA_DOMAIN)
    h.request('POST', "/in.php", body, headers)
    resp = h.getresponse()
    data = resp.read()
    h.close()
    if resp.status == 200:
        cap_id = int(data.split('|')[1])
        return cap_id
    else:
        return False