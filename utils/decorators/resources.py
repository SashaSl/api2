# -*- coding: utf-8 -*-

import resource


def limit_resources(max_memory=1*2**30):
    def _decorator(func):
        def wrapper(*args, **kwargs):
            old_res = resource.getrlimit(resource.RLIMIT_AS)
            try:
                resource.setrlimit(resource.RLIMIT_AS, (max_memory, resource.RLIM_INFINITY))
                return func(*args, **kwargs)
            finally:
                resource.setrlimit(resource.RLIMIT_AS, old_res)
        return wrapper
    return _decorator