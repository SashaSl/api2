# -*- coding: utf-8 -*-

import os
import time
from pyvirtualdisplay import Display


def Xvfb(visible=1, width=800, height=600, timeout=30):
    if visible:
        os.environ['DISPLAY'] = ':0'

    def decorator(func):
        def wrapper(*args, **kwargs):
            display = Display(visible=visible, size=(width, height))
            display.start()
            time.sleep(timeout)
            try:
                result = func(*args, **kwargs)
            finally:
                display.stop()
            return result
        return wrapper
    return decorator