import socket


def connection_timeout(timeout):
    def timeout_decorator(func):
        def wrapper(*args, **kwargs):
            _timeout = socket.getdefaulttimeout()
            socket.setdefaulttimeout(timeout)
            result = func(*args, **kwargs)
            socket.setdefaulttimeout(_timeout)
            return result
        return wrapper
    return timeout_decorator