# -*- coding: utf-8 -*-

from functools import wraps
from flask import request, jsonify
from utils.errors import error_to_string, StringError


def catch_errors(func):
    @wraps(func)
    def _decorator(*args, **kwargs):
        try:
            return {
                'success': True,
                'result': func(*args, **kwargs),
                'error': None
            }
        except:
            return {
                'success': False,
                'result': None,
                'error': error_to_string()
            }
    return _decorator


def check_content_type(func):
    @wraps(func)
    def _decorator(*args, **kwargs):
        if request.json is None:
            raise StringError('Incorrect content type. Set it to application/json!')
        return func(*args, **kwargs)
    return _decorator


def json_view(func):
    @wraps(func)
    def _decorator(*args, **kwargs):
        _func = catch_errors(check_content_type(func))
        return jsonify(**_func(*args, **kwargs))
    return _decorator
