# -*- coding: utf-8 -*-

import os
import time
import socket
import random
import signal

import cfg.common

TOR_ON = cfg.common.tor_on


class TorWrapper(object):
    TMP = "/tmp"
    TOR_DATA = TMP + "/{port}.tor.datadir"
    TOR_PID = TMP + "/{port}.tor.pid"
    TOR_STDOUT = TMP + "/{port}.tor.stdout"
    PRIVOXY_PID = TMP + "/{port}.privoxy.pid"
    PRIVOXY_CFG = TMP + "/{port}.privoxy.cfg"
    TOR_CMD = "tor SocksPort {port} RunAsDaemon 0 Log info DataDirectory '{datadir}' > '{outfile}' & echo $! > {pidfile}"
    TOR_READY = "Bootstrapped 100%"
    PRIVOXY_CFG_TEMPLATE = "forward-socks4a / 127.0.0.1:{tor_port} .\nlisten-address  127.0.0.1:{privoxy_port}"
    PRIVOXY_CMD = "privoxy --no-daemon '{cfgfile}' > /dev/null 2>/dev/null & echo $! > {pidfile}"

    @staticmethod
    def _tor_version():
        return os.popen("tor --version | awk '/^Tor version/{print $3}'").read().strip()

    @staticmethod
    def port_in_use(port):
        print "Checking port %d..." % port
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind(('127.0.0.1', port))
            sock.close()
            return False
        except socket.error, e:
            return True

    @staticmethod
    def generate_free_port(port_range=[1000, 32767]):
        print "Searching for free ports."
        port = random.randint(*port_range)
        while TorWrapper.port_in_use(port):
            port = random.randint(*port_range)
        return port

    @property
    def tor_port(self):
        if not hasattr(self, "_tor_port"):
            self._tor_port = self.generate_free_port()
        return self._tor_port

    @property
    def privoxy_port(self):
        if not hasattr(self, "_privoxy_port"):
            self._privoxy_port = self.generate_free_port()
        return self._privoxy_port

    def _is_tor_ready(self):
        print "Checking if Tor is up..."
        try:
            return self.TOR_READY in open(self.TOR_STDOUT.format(port=self.tor_port)).read()
        except IOError:
            return False

    def _is_privoxy_ready(self):
        print "Checking if Privoxy is up..."
        time.sleep(3)
        return True

    def _run_tor(self):
        print "Creating Tor data directory."
        datadir = self.TOR_DATA.format(port=self.tor_port)

        if self._tor_version() < "0.2.3":
            print "Old Tor."
            user = "debian-tor"
        else:
            print "New Tor."
            user = "root"
        os.system("sudo -u {user} mkdir -p -m 777 '{datadir}'".format(datadir=datadir, user=user))

        print "Starting Tor."
        cmd = self.TOR_CMD.format(
            port=self.tor_port,
            datadir=self.TOR_DATA.format(port=self.tor_port),
            pidfile=self.TOR_PID.format(port=self.tor_port),
            outfile=self.TOR_STDOUT.format(port=self.tor_port)
        )
        os.system(cmd)

    def _run_privoxy(self):
        print "Creating Privoxy config file."
        with open(self.PRIVOXY_CFG.format(port=self.tor_port), "w") as config:
            print >> config,  self.PRIVOXY_CFG_TEMPLATE.format(
                tor_port=self.tor_port,
                privoxy_port=self.privoxy_port
            )

        print "Starting Privoxy."
        cmd = self.PRIVOXY_CMD.format(
            pidfile=self.PRIVOXY_PID.format(port=self.tor_port),
            cfgfile=self.PRIVOXY_CFG.format(port=self.tor_port)
        )
        os.system(cmd)

    @property
    def tor_pid(self):
        try:
            if not hasattr(self, '_tor_pid'):
                print "Getting Tor pid from pid file."
                self._tor_pid = int(open(self.TOR_PID.format(port=self.tor_port)).read().strip())
                return self._tor_pid
            return self._tor_pid
        except IOError:
            return None

    @property
    def privoxy_pid(self):
        try:
            if not hasattr(self, '_privoxy_pid'):
                print "Getting Privoxy pid from pid file."
                self._privoxy_pid = int(open(self.PRIVOXY_PID.format(port=self.tor_port)).read().strip())
                return self._privoxy_pid
            return self._privoxy_pid
        except IOError:
            return None

    def _is_tor_alive(self):
        print "Checking if Tor process exists."
        if self.tor_pid is None:
            return False
        return self._check_pid(self.tor_pid)

    def _is_privoxy_alive(self):
        print "Checking if Privoxy process exists."
        if self.privoxy_pid is None:
            return False
        return self._check_pid(self.privoxy_pid)

    def _clean(self):
        print "Cleaning."
        items = [
            self.TOR_DATA.format(port=self.tor_port),
            self.TOR_PID.format(port=self.tor_port),
            self.TOR_STDOUT.format(port=self.tor_port),
            self.PRIVOXY_CFG.format(port=self.tor_port),
            self.PRIVOXY_PID.format(port=self.tor_port)
        ]
        for i in items:
            if os.path.exists(i):
                print "Removing " + i + "..."
                os.system("rm -rf '%s'" % i)

    @staticmethod
    def _check_pid(pid):
        try:
            os.kill(pid, 0)
            return True
        except OSError:
            return False

    def _kill_tor(self):
        print "Killing Tor."
        while self._is_tor_alive():
            print "Trying to kill Tor by pid..."
            os.kill(self.tor_pid, signal.SIGKILL)
            time.sleep(1)

    def _kill_privoxy(self):
        print "Killing Privoxy."
        while self._is_privoxy_alive():
            print "Trying to kill Privoxy by pid..."
            os.kill(self.privoxy_pid, signal.SIGKILL)
            time.sleep(1)

    @staticmethod
    def wait(condition, timeout):
        ts = time.time()
        while time.time() - ts <= timeout:
            if condition():
                return True
            time.sleep(1)
        return False

    def stop(self):
        print "Stopping."
        if hasattr(self, "_cleaned"):
            return self._cleaned

        self._kill_tor()
        self._kill_privoxy()
        self._clean()
        self._cleaned = True
        return self._cleaned

    def __del__(self):
        self.stop()

    def start(self, timeout):
        print "Starting."
        self._run_tor()
        if not self.wait(self._is_tor_alive, timeout):
            self.stop()
            return False
        if not self.wait(self._is_tor_ready, timeout):
            self.stop()
            return False

        self._run_privoxy()
        if not self.wait(lambda: self._is_privoxy_alive, timeout):
            self.stop()
            return False
        if not self.wait(lambda: self._is_privoxy_ready, timeout):
            self.stop()
            return False
        return True


def Tor(replace_only_none_proxy=True, wait_for_tor_ready_timeout=60, require_proxy_kwarg=False):
    def Tor_decorator(func):
        def wrapper(*args, **kwargs):
            if TOR_ON:
                if 'tor_disable' in kwargs and kwargs['tor_disable'] is True:
                    result = func(*args, **kwargs)
                else:
                    if 'proxy' not in kwargs:
                        if require_proxy_kwarg:
                            raise Exception("There is no 'proxy=...' parameter in kwargs!")
                        else:
                            kwargs['proxy'] = None
                    proxy = kwargs['proxy']
                    if replace_only_none_proxy and proxy is not None:
                        return func(*args, **kwargs)

                    while True:
                        try:
                            tw = TorWrapper()
                            if tw.start(wait_for_tor_ready_timeout):
                                break
                        except:
                            tw.stop()

                    if 'tor_pid' in kwargs:
                        kwargs['tor_pid'] = tw.tor_pid

                    kwargs['proxy'] = '127.0.0.1:%d' % tw.privoxy_port

                    try:
                        result = func(*args, **kwargs)
                    except:
                        tw.stop()
                        raise
            else:
                result = func(*args, **kwargs)

            return result
        return wrapper
    return Tor_decorator