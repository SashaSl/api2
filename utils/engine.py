# -*- coding: utf-8 -*-

class Engine(object):
    ENGINE_YANDEX = "yandex"
    ENGINE_DIRECT_API = "direct_api"
    ENGINE_GOOGLE = "google"
    ENGINE_BING = "bing"
    ENGINE_MAIL = "mail"
    ENGINE_YOUTUBE = "youtube"