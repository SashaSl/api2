# -*- coding: utf-8 -*-

import time
import dateutil.parser


def datetime_to_ts(dt):
    return int(time.mktime(dt.timetuple()))


def try_to_datetime(s):
    try:
        return dateutil.parser.parse(s)
    except:
        return s