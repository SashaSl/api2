import mechanize
import cookielib


class Browser(object):
    def __init__(self, proxy = None, useragent = None):
        if useragent is None:
            self.useragent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 YaBrowser/1.7.1364.21023 Safari/537.22'
        else:
            self.useragent = useragent

        browser = mechanize.Browser()
        self.browser = browser

        cookies = cookielib.LWPCookieJar()
        browser.set_cookiejar(cookies)

        browser.set_handle_equiv(True)
        browser.set_handle_gzip(True)
        browser.set_handle_redirect(True)
        browser.set_handle_referer(True)
        browser.set_handle_robots(False)
        browser.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

        browser.addheaders = [('User-agent', self.useragent)]
        self.set_proxy(proxy)

        browser._factory.is_html = True

    def set_proxy(self, proxy):
        if proxy is None:
            self.proxy = None
        elif isinstance(proxy, basestring):
            self.proxy = proxy
        else:
            raise TypeError

        if self.proxy:
            self.browser.set_proxies({"http": self.proxy})

    def user_profile(self):
        cookiestr = ''
        cookie_list = []
        for c in self.browser._ua_handlers['_cookies'].cookiejar:
            cookie_list.append({
                'name': c.name,
                'value': c.value,
                'domain': c.domain,
            })

        return {
            'cookie': cookie_list,
            'proxy': self.proxy,
            'useragent': self.useragent
        }
