# -*- coding: utf-8 -*-

import re
from db.common import DB


def extract_region(region):
    if not isinstance(region, basestring):
        return region

    mask1 = re.compile('^' + region + '$', re.IGNORECASE)
    mask2 = re.compile(region, re.IGNORECASE)
    return DB.db.region.find_one({
        '$or': [
            {'name_ru': mask1},
            {'name_en': mask1},
            {'name': mask1}
        ]
    }) or DB.db.region.find_one({
        '$or': [
            {'name_ru': mask2},
            {'name_en': mask2},
            {'name': mask2}
        ]
    })


def extract_and_canonize_region(region):
    return extract_region(region or 'moscow')['name']


def engineregionalparams(region, engine):
    return DB.db.engineregionalparams.find_one({
        'region_name': region,
        'engine': engine
    })