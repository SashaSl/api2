# -*- coding: utf-8 -*-

import copy
import pymongo
from bson.objectid import ObjectId

import cfg.mongo
from mongodb_proxy import MongoProxy
from utils.datetime import try_to_datetime


class LazyDb(object):
    def __init__(self):
        self._conn = None

    @property
    def conn(self):
        if self._conn is None:
            conn = pymongo.MongoClient(cfg.mongo.mongodb_url)
            self._conn = MongoProxy(conn)
        return self._conn

    @property
    def db(self):
        return self.conn[cfg.mongo.mongodb_dbname]


DB = LazyDb()


def json_fix_for_mongo(obj):
    def _json_fix_datetime(obj):
        if isinstance(obj, list):
            for i, subobj in enumerate(obj):
                obj[i] = json_fix_for_mongo(subobj)
        elif isinstance(obj, dict):
            for key, val in obj.iteritems():
                if key == "_id" and isinstance(val, basestring):
                    obj[key] = ObjectId(val)
                elif key == "_id" and isinstance(val, dict) and '$in' in val:
                    obj[key]['$in'] = map(ObjectId, val['$in'])
                else:
                    obj[key] = json_fix_for_mongo(val)
        elif isinstance(obj, basestring):
            obj = try_to_datetime(obj)
        return obj
    return _json_fix_datetime(copy.deepcopy(obj))