# -*- coding: utf-8 -*-

import random
import string


def random_string(length=8):
    return ''.join(map(lambda i: random.choice('qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890'), xrange(length)))


def punct_remover(st):
    for item in string.punctuation:
        st = st.replace(item, ' ')
    return st.split()