# -*- coding: utf-8 -*-

import re
from HTMLParser import HTMLParser, HTMLParseError


def extract_domain(url):
    domains = re.findall(ur'^(?:(?:https?:)?//)?(?:www\.)?([^/?#\s]*)', url.lower())
    return domains[0].encode('idna')


def strip_tags(html):
    class MLStripper(HTMLParser):
        def __init__(self):
            self.reset()
            self.fed = []

        def handle_data(self, d):
            self.fed.append(d)

        def get_data(self):
            return ''.join(self.fed)
    s = MLStripper()
    try:
        s.feed(html)
    except HTMLParseError:
        return ''
    return s.get_data()


def link_preparator(raw_html):
    return raw_html.split('url=')[1]


def extract_domain_without_encoding(url):
    domains = re.findall(ur'^(?:(?:https?:)?//)?(?:www\.)?([^/?#\s]*)', url.lower())
    return domains[0]


def extract_first_domain(url):
    domains = re.findall(ur'^(?:(?:https?:)?//)?(?:www\.)?([^/?#\s]*)', url.lower())
    return domains[0].split('.')[-2] + '.' + domains[0].split('.')[-1]