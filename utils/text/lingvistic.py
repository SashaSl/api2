# -*- coding: utf-8 -*-

import re
from pymorphy2 import MorphAnalyzer


class CachingMorphAnalyzer(MorphAnalyzer):
    _cache_parse = {}

    def parse(self, word):
        if word in self._cache_parse:
            return self._cache_parse[word]
        self._cache_parse[word] = super(self.__class__, self).parse(word)
        return self._cache_parse[word]

    def word_normalizer(self, word):
        morph_word = self.parse(word)
        if morph_word:
            return morph_word[0].normal_form
        else:
            return word


class Morph(object):
    space = re.compile(u'\s+')
    cache = {}

    def __init__(self, use_cache=True):
        import pymorphy2
        self.morph = pymorphy2.MorphAnalyzer()
        self.use_cache = use_cache

    def norm_to_list(self, text):
        words = []
        for word in self.space.split(text):
            if not word: continue
            norm = None
            if self.use_cache:
                norm = self.cache.get(word, None)
            if norm is None:
                norm = self.morph.parse(word)
                norm = norm[0].normal_form if norm else word
                if self.use_cache:
                    self.cache[word] = norm
            words.append(norm)
        return words

    def norm_to_set(self, text):
        return set(self.norm_to_list(text))

    def norm_to_string(self, text):
        return u' '.join(self.norm_to_list(text))