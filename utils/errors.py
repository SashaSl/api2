# -*- coding: utf-8 -*-

from __future__ import absolute_import

import sys
import datetime
import traceback

from .db.common import DB


class StringError(Exception):
    """
    Этот тип исключения нужен чтобы @json_view вернул ошибку не в виде трэйсбэка, а просто строку.
    """
    pass


def error_to_string():
    exc_type, exc_value, exc_traceback = sys.exc_info()
    if exc_type is StringError:
        return exc_value.message
    return ''.join(traceback.format_exception(exc_type, exc_value, exc_traceback))


def error_to_db():
    DB.db.errors.insert({
        'created': datetime.datetime.now(),
        'error': error_to_string()
    })