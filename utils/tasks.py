# -*- coding: utf-8 -*-

from __future__ import absolute_import

import datetime
from celery.utils import uuid
from celery import chord, Task
from collections import defaultdict

import cfg.common
from .db.common import DB, LazyDb
from applications.celery import app

SLEEP_TMT = cfg.common.wait_for_tasks_timeout


class BaseTask(Task):
    abstract = True
    _db = LazyDb()

    @property
    def group_id(self):
        return get_group(self.request.id)

    def __call__(self, *args, **kwargs):
        self.set_progress(0.0)
        return super(BaseTask, self).__call__(*args, **kwargs)

    def on_success(self, *args, **kwargs):
        self.set_progress(1.0)
        return super(BaseTask, self).on_success(*args, **kwargs)

    def on_retry(self, *args, **kwargs):
        self.set_progress(0.0)
        return super(BaseTask, self).on_retry(*args, **kwargs)

    def on_failure(self, *args, **kwargs):
        self.set_progress(0.0)
        return super(BaseTask, self).on_failure(*args, **kwargs)

    def set_progress(self, progress):
        set_progress(self.request.id, self.group_id, progress)

    @property
    def db(self):
        return self._db.db


def save_result(collection, result_type, arguments, result):
    item = {
        "type": result_type,
        "created": datetime.datetime.now(),
        "arguments": arguments,
        "result": result
    }
    collection.insert(item)
    return item


def get_result(collection, result_type, arguments, cache_timeout):
    res = collection.find_one({
        '$query': {
            'created': {
                '$gte': datetime.datetime.now() - datetime.timedelta(seconds=cache_timeout)
            },
            'type': result_type,
            'arguments': arguments
        },
        '$orderby': {
            'created': -1
        }
    })
    return res


def set_progress(task_id, group_id, progress):
    if group_id is None:
        return False
    progress = float(progress)
    assert 0.0 <= progress <= 1.0
    collection = DB.db.celery_group_meta
    collection.ensure_index([
        ('group_id', 1),
        ('task_id', 1)
    ], unique=True)
    collection.update(
        {'group_id': group_id, 'task_id': task_id},
        {'$set': {'progress': progress, 'updated': datetime.datetime.now()}},
        upsert=True
    )
    return True


def get_group(task_id):
    item = DB.db.celery_group_meta.find_one({'task_id': task_id})
    return item['group_id'] if item else None


def cancel_task(task_id):
    group_id = get_group(task_id)
    return cancel_group(task_id) | cancel_group(group_id)


def cancel_group(group_id):
    if group_id is None:
        return set()

    group_progress = get_group_progress(group_id)
    tasks_ids = [group_id]
    if group_progress:
        for subtask in group_progress['tasks']:
            tasks_ids.append(subtask['task_id'])

    result = set()
    for task_id in tasks_ids:
        async_result = app.AsyncResult(task_id)
        async_result.revoke(terminate=True)  # signal=signal.SIGQUIT)
        result.add(task_id)
    return result


def get_group_progress(group_id):
    if group_id is None:
        return None
    items = list(DB.db.celery_group_meta.find({'group_id': group_id}))
    if not items:
        return None
    result = defaultdict(list)
    result['group_id'] = group_id
    for item in items:
        async_result = app.AsyncResult(item['task_id'])
        task_result = async_result.result if async_result.ready() else None
        task_state = async_result.state
        result['tasks'].append({
            'task_id': item['task_id'],
            'progress': item['progress'],
            'result': task_result,
            'state': task_state.lower()
        })
        result['progress'].append(item['progress'])
    result['progress'] = float(sum(result['progress'])) / len(result['progress']) if result['progress'] else 0.0
    return result


def register_in_group(task_id, group_id):
    set_progress(task_id, group_id, 0.0)


def progress_chord(subtasks, callback, track_progress=True):
    if not track_progress:
        return chord(subtasks)(callback)
    callback_task_id = uuid()
    _subtasks = []
    for subtask in subtasks:
        task_id = uuid()
        _subtask = subtask.clone().set(task_id=task_id)
        _subtasks.append(_subtask)
        register_in_group(task_id, callback_task_id)

    _callback = callback.clone().set(task_id=callback_task_id)
    result = chord(_subtasks)(_callback)
    return result


def progress_task(task):
    group_id = uuid()
    task_id = uuid()
    _task = task.s().set(task_id=task_id)
    register_in_group(task_id, group_id)
    return _task


def get_chunks(it, chunk_size):
    chunk = []
    for i in it:
        chunk.append(i)
        if len(chunk) >= chunk_size:
            yield chunk
            chunk = []
    if chunk:
        yield chunk


def check_if_parsed(check_list, collection, result_type, arguments, cache_timeout):
    list_to_parse = []
    if result_type == 'wordstat':
        wordstat_maxpage = arguments['wordstat_maxpage']
        del arguments['wordstat_maxpage']
        for item in check_list:
            for page in xrange(wordstat_maxpage):
                arguments['request'] = item['request']
                arguments['page'] = page + 1
                res = get_result(collection, result_type, arguments, cache_timeout)
                if not res:
                    list_to_parse.append({
                        'request': item['request'],
                        'page': page + 1
                    })
    else:
        for item in check_list:
            arguments['request'] = item['request']
            res = get_result(collection, result_type, arguments, cache_timeout)
            if not res:
                list_to_parse.append(item)
    return list_to_parse