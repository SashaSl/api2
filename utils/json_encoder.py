# -*- coding: utf-8 -*-

from __future__ import absolute_import

from datetime import datetime
from bson.objectid import ObjectId
from flask.json import JSONEncoder


class ApiJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, datetime):
                return obj.isoformat()
            elif isinstance(obj, ObjectId):
                return str(obj)
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return super(ApiJSONEncoder, self).default(self, obj)