#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cfg.gunicorn
from applications.flask import app
import views
import middlewares


# URL routing
app.route('/api/task/info', methods=['POST'])(views.task.task_info)
app.route('/api/task/cancel', methods=['POST'])(views.task.task_cancel)
app.route('/api/db/find/<collection>', methods=['POST'])(views.db.db_find)
app.route('/api/db/find_one/<collection>', methods=['POST'])(views.db.db_find_one)
app.route('/api/parser/suggest', methods=['POST'])(views.parser.suggest.parser_suggest)
app.route('/api/yandex/direct_search', methods=['POST'])(views.yandex.direct_search.yandex_direct_search)
app.route('/api/yandex/budget', methods=['POST'])(views.yandex.budget.yandex_budget)


# Middlewares
app.before_request(middlewares.authentication_middleware)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=cfg.gunicorn.api_port, ssl_context='adhoc')