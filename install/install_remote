#!/bin/bash

[ "on" == "$E_API_DEBUG" ] && set -x  # turn on debug output
set -e

print_usage() {
    echo "Usage: $0 [-c|--clean] [-f|--fast] [-b|--branch BRANCH] --git-user GIT_USER --git-password GIT_PASSWORD
    -c|--clean: Drop project directory and clone clean repository before installation. WARNING: this is very dangerous option!
    -f|--fast: Not actually install. Just switch to target branch and pull.
    -b|--branch BRANCH: Specify target branch (default: master).
    --git-user GIT_USER: Specify git login.
    --git-password GIT_PASSWORD: Specify git password.
    "
}

install() {
    role="$1"
    host="$2"
    user="$3"
    pass="$4"
    br="$5"
    args="$6"
    cln="$7"
    fst="$8"
    gu="$9"
    gp="${10}"

    echo "Install $role..."
    read -r -p "Skip(yes/no): " -e -i "no" skip
    if [ "yes" = "$skip" ] ; then
        return
    else
        if [ "no" != "$skip" ] ; then
            exit 1
        fi
    fi

    read -r -p "Enter role for $role server: " -e -i "$role" role
    read -r -p "Enter IP or Hostname for $role server: " -e -i "$host" host
    read -r -p "Enter ssh user for $role server: " -e -i "$user" user
    read -r -p "Enter ssh password for $role server: " -e -i "$pass" pass
    read -r -p "Enter deployment branch for $role server: " -e -i "$br" br
    read -r -p "Enter extra arguments to pass to install script for $role server: " -e -i "$args" args
    read -r -p "Enter clean or not for $role server: " -e -i "$cln" cln
    read -r -p "Enter fast or not for $role server: " -e -i "$fst" fst

    if [ "no" = "$fst" ] ; then
        script="
            set -xe
            cd /opt &&
            apt-get -y update &&
            apt-get -y install git-core psmisc &&

            service api_celeryd stop || true
            service api_celerybeat stop || true
            service api_gunicorn stop || true
            service api_flower stop || true

            if [ "yes" = "$cln" ] ; then
                rm -rf e-api2 || true
                git clone https://$gu:$gp@bitbucket.org/evristic/e-api2.git
            fi

            [ -d "e-api2" ] || git clone https://$gu:$gp@bitbucket.org/evristic/e-api2.git

            cd e-api2 &&
            git reset --hard &&
            git checkout master &&
            git pull &&
            git checkout $br &&
            git pull &&
            install/install $args --auto -t $role &&

            service api_celeryd start || true
            service api_celerybeat start || true
            service api_gunicorn start || true
            service api_flower start || true

            reboot
        "
    else
        script="
            set -xe
            cd /opt

            if [ "yes" = "$cln" ] ; then
                rm -rf e-api2 || true
                git clone https://$gu:$gp@bitbucket.org/evristic/e-api2.git
            fi

            [ -d "e-api2" ] || git clone https://$gu:$gp@bitbucket.org/evristic/e-api2.git

            cd e-api2 &&
            git reset --hard &&
            git checkout master &&
            git pull &&
            git checkout $br &&
            git pull &&

            service api_celeryd restart || true
            service api_celerybeat restart || true
            service api_gunicorn restart || true
            service api_flower restart || true
        "
    fi

    sshpass -p "$pass" ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -C $user@$host -t "
        cd /opt &&
        echo \"$script\" > install &&
        chmod a+x install &&
        ./install
    "
    echo
    echo
    echo
}

branch=master
clean=no
fast=no
git_user=
git_password=

while [[ $# > 0 ]] ; do
    key="$1"
    shift

    case $key in
        -c|--clean)
            clean="yes"
            ;;
        -b|--branch)
            branch="$1"
            shift
            ;;
        --git-user)
            git_user="$1"
            shift
            ;;
        --git-password)
            git_password="$1"
            shift
            ;;
        -f|--fast)
            fast="yes"
            ;;
        *)
            print_usage
            exit 1
            ;;
    esac
done

( [ -z "$git_user" ] || [ -z "$git_password" ] ) && print_usage && exit 1

echo "Install e-api2."
install master api2.evristic.ru    root pravsecretkey11 $branch "" $clean $fast $git_user $git_password
install worker api2-01.evristic.ru root pravsecretkey11 $branch "" $clean $fast $git_user $git_password
install worker api2-02.evristic.ru root pravsecretkey11 $branch "" $clean $fast $git_user $git_password