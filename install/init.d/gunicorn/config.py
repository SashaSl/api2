import os
import sys

ROOT_DIR = os.path.dirname(__file__)
ROOT_DIR = os.path.abspath(os.path.join(ROOT_DIR, '../../..'))
sys.path.append(ROOT_DIR)

import cfg.gunicorn

bind = ":" + str(cfg.gunicorn.api_port)
workers = cfg.gunicorn.gunicorn_workers
max_requests = cfg.gunicorn.gunicorn_max_requsts
timeout = cfg.gunicorn.gunicorn_timeout
graceful_timeout = cfg.gunicorn.gunicorn_graceful_timeout
keepalive = cfg.gunicorn.gunicorn_keepalive
limit_request_line = cfg.gunicorn.gunicorn_limit_request_line
limit_request_fields = cfg.gunicorn.gunicorn_limit_request_fields
limit_request_field_size = cfg.gunicorn.gunicorn_limit_request_fields
debug = cfg.gunicorn.gunicorn_debug
chdir = cfg.gunicorn.gunicorn_chdir
pythonpath = cfg.gunicorn.gunicorn_pythonpath
loglevel = cfg.gunicorn.gunicorn_loglevel
access_log_format = cfg.gunicorn.gunicorn_access_log_format
proc_name = cfg.gunicorn.gunicorn_proc_name
default_proc_name = cfg.gunicorn.gunicorn_default_proc_name
certfile = cfg.gunicorn.gunicorn_certfile
keyfile = cfg.gunicorn.gunicorn_keyfile