# -*- coding: utf-8 -*-

from flask import request

import cfg.common
import cfg.celery
import tasks.parser.suggest
from utils.engine import Engine
from utils.tasks import get_chunks
from utils.tasks import progress_chord
from utils.decorators.views import json_view

SUGGEST_CHUNK_SIZE = cfg.common.suggest_chunk_size


@json_view
def parser_suggest():
    """
    request.json = {
        "items": [
            {
                "request": "...",
                "prev_request": null|"..."
            },
            ...
        ],
        "region": "...",
        "engine": "...",
        "queue": "...",
        "iterations": int,
        "cache_timeout": int,
    }
    """
    items = request.json['items']
    region = request.json.get('region', 'moscow')
    engine = request.json.get('engine', Engine.ENGINE_YANDEX)
    queue = request.json.get('queue', cfg.celery.CELERY_DEFAULT_QUEUE)
    iterations = request.json.get('iterations', 1)
    cache_timeout = request.json.get('cache_timeout', 3600)

    subtask_template = tasks.parser.suggest.parser_suggest_chunk.s(
        engine,
        region,
        iterations=iterations,
        cache_timeout=cache_timeout,
    ).set(queue=queue)

    # При клонировании частичных сигнатур с указанием позиционных аргументов - эти аргументы добавляются в
    # начало имеющихся позиционных аргументов сигнатуры! Т.е. в данном случае в итоге получим
    # args = (items_chunk, engine, region)
    subtasks = [subtask_template.clone(args=(items_chunk,)) for items_chunk in get_chunks(items, SUGGEST_CHUNK_SIZE)]
    callback = tasks.parser.suggest.parser_suggest_merge_chunks.s().set(queue=queue)

    chord_result = progress_chord(subtasks, callback)

    # Если прогресс по группе не нужен, можно использовать такой вариант:
    # >>> from utils.tasks import progress_chord
    # >>> chord_result = progress_chord(subtasks, callback, track_progress=False)
    # Однако в этом случае, кроме отсутствия прогресса ещё исчезнет возможность килять всю группу тасков!

    task_id = chord_result.id

    # Если нужно запустить одиночную задачу с поддержкой прогресса, используйте такой код:
    # >>> from utils.tasks import progress_task
    # >>> async_result = progress_task(some_task).apply_async(args=[...], kwargs={...}, ...)

    # Если вдруг надо вернуть ошибку в читабельном виде, делаем так:
    # >>> from utils.errors import StringError
    # >>> raise StringError(u'Что-то там сломалось')
    # В этом случае вьюшка вернёт json с success = False и error = u'Что-то там сломалось'

    return {
        "task_id": task_id
    }