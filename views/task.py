# -*- coding: utf-8 -*-

from flask import request

from utils.db.common import DB
from applications.celery import app
from utils.decorators.views import json_view
from utils.tasks import get_group_progress, cancel_task


@json_view
def task_info():
    task_id = request.json['task_id']
    async_result = app.AsyncResult(task_id)

    progress = get_group_progress(task_id)
    status = async_result.status.lower()
    result = {
        'status': status,
        'result': async_result.result if async_result.ready() else None,
        'progress': progress,
    }
    return result


@json_view
def task_cancel():
    task_id = request.json['task_id']
    return {
        'tasks': [{'task_id': _task_id} for _task_id in cancel_task(task_id)]
    }