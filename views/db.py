# -*- coding: utf-8 -*-

from flask import request

from utils.db.common import DB
from utils.decorators.views import json_view
from utils.db.common import json_fix_for_mongo


@json_view
def db_find(collection):
    query = json_fix_for_mongo(request.json)
    return DB.db[collection].find(query)


@json_view
def db_find_one(collection):
    query = json_fix_for_mongo(request.json)
    return DB.db[collection].find_one(query)