# -*- coding: utf-8 -*-

from flask import request

import cfg.celery
import cfg.common
import tasks.yandex.budget
from utils.decorators.views import json_view

DEFAULT_QUEUE = cfg.celery.CELERY_DEFAULT_QUEUE
DEFAULT_CACHE_TIMEOUT = cfg.common.default_cache_timeout

@json_view
def yandex_budget():
    """
    request.json = {
        "items": [
            {
                "request": "...",
            },
            ...
        ],
        "region": "...",
        "queue": "{default|low|high}",
        "cache_timeout": int,
    }
    """
    items = request.json['items']
    region = request.json.get('region', 'moscow')

    queue = request.json.get('queue', DEFAULT_QUEUE)
    cache_timeout = request.json.get('cache_timeout', 0)
    task = tasks.yandex.budget.yandex_budget.apply_async(args=[items, region],
                                                      kwargs={'queue': queue,
                                                              'cache_timeout': cache_timeout},
                                                      queue=queue)
    task_id = task.task_id
    return {
        "task_id": task_id
    }