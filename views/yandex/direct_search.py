# -*- coding: utf-8 -*-

from flask import request

import cfg.celery
import cfg.common
import tasks.yandex.direct_search
from utils.decorators.views import json_view
from utils.tasks import get_chunks, progress_chord

DEFAULT_QUEUE = cfg.celery.CELERY_DEFAULT_QUEUE
DIRECT_CHUNK_SIZE = cfg.common.direct_search_chunk_size
DEFAULT_CACHE_TIMEOUT = cfg.common.default_cache_timeout

@json_view
def yandex_direct_search():
    """
    request.json = {
        "items": [
            {
                "request": "...",
            },
            ...
        ],
        "region": "...",
        "queue": "...",
        "max_direct_pos": int,
        "cache_timeout": int,
    }
    """
    items = request.json['items']
    region = request.json.get('region', 'moscow')
    queue = request.json.get('queue', DEFAULT_QUEUE)
    max_direct_search_pos = request.json.get('max_direct_search_pos', None)
    cache_timeout = request.json.get('cache_timeout', DEFAULT_CACHE_TIMEOUT)

    subtask_template = tasks.yandex.direct_search.yandex_direct_search_chunk.s(
        region,
        max_direct_search_pos=max_direct_search_pos,
        cache_timeout=cache_timeout,
    ).set(queue=queue)

    subtasks = [subtask_template.clone(args=(items_chunk,)) for items_chunk in get_chunks(items, DIRECT_CHUNK_SIZE)]
    callback = tasks.yandex.direct_search.yandex_direct_search_merge_chunks.s().set(queue=queue)

    chord_result = progress_chord(subtasks, callback)
    task_id = chord_result.id
    return {
        "task_id": task_id
    }