# -*- coding: utf-8 -*-

import os
import sys
import datetime

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from utils.db.common import DB

collection = DB.db.celery_group_meta

collection.remove({
    'updated': {
        '$lte': datetime.datetime.now() - datetime.timedelta(days=2)
    }
})