# -*- coding: utf-8 -*-

import cfg.flask
from flask import request, redirect, Response


def authentication_middleware():
    auth = request.authorization
    if not auth or not (auth.username == cfg.flask.api_http_user and auth.password == cfg.flask.api_http_password):
        return Response(
            'Could not verify your access level for that URL.\nYou have to login with proper credentials',
            401,
            {'WWW-Authenticate': 'Basic realm="Login Required"'}
        )