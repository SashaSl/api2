# -*- coding: utf-8 -*-

import json
import requests

class ApiResult(object):
    def __init__(self, data):
        self.__json = data

    def to_json(self):
        return self.__json

    def __invert__(self):
        return self.__json

    def __wrap_api_result(self, item):
        value = self.__json[item]
        if isinstance(value, (list, dict)):
            return ApiResult(value)
        return value

    def __getattr__(self, item):
        try:
            return self.__wrap_api_result(item)
        except:
            raise AttributeError

    def __getitem__(self, item):
        if not isinstance(item, (basestring, int)):
            raise TypeError
        try:
            return self.__wrap_api_result(item)
        except:
            if isinstance(item, basestring):
                raise KeyError
            elif isinstance(item, int):
                raise IndexError
            raise


class Api(object):
    """
    Example of usage:

    >>> from api_interface import Api
    >>> api = Api('evristic', 'FOcZ4AZ4yXU7m00bnjQvcn5llYzE1fz0', 'https://127.0.0.1:8080')
    >>> ar = api('api/task/info', {'task_id': '97898bd6-613d-412c-a2be-41ba43c25312'})
    >>> ~ar.result.progress.tasks[0].result[2].result.suggest
    [u'\u043f\u0440\u0438\u0432\u0435\u0442', ...]
    """

    def __init__(self, login, password, base_url='https://api2.evristic.ru', chunk_size=50):
        self.base_url = base_url.rstrip('/') + '/'
        self.login = login
        self.password = password
        self.chunk_size = chunk_size
        self.ids = set()
        self.results = []
        self.downloaded_ids = []

    def __call__(self, path, data, extract_result=True):
        """
        path -- For example, /api/parser/suggest or api/parser/suggest.
        data -- dict with input parameters.
        """
        url = self.base_url + path.lstrip('/')
        auth = (self.login, self.password)
        json_data = json.dumps(data, indent=4)
        headers = {'content-type': 'application/json'}
        response = requests.post(url, data=json_data, auth=auth, headers=headers, verify=False).json()
        if extract_result:
            response = self._process_response(response)
        # ApiResult is quite useful. For example:
        # x = {'qwe': [1, 2, [1, 2, {'asd': 12}]]}
        # r = ApiResult(x)
        # r.qwe[2][2].asd  # return 12
        # r.qwe[2][2].to_json()  # return {'asd': 12}
        # ~r.qwe[2][2]  # the same, return {'asd': 12}

        return ApiResult(response)

    def _process_response(self, resp):
        self._collect_ids(resp)
        self._get_results()
        resp = self._replace_ids(resp)
        return resp

    def _get_results(self):
        ids_to_download = filter(lambda i: i not in self.downloaded_ids, list(self.ids))
        if ids_to_download:
            chunks = self._get_chunks(ids_to_download, self.chunk_size)
            for chunk in chunks:
                chunk_result = self.__call__('/api/db/find/results', {'_id': {'$in': chunk}}, extract_result=False).to_json()
                self.results += chunk_result['result']
            self.downloaded_ids += ids_to_download

    def _collect_ids(self, node):
        if not node:
            return
        for key, value in node.items():
            if not value:
                continue
            elif isinstance(value, dict):
                if '_id' in value:
                    self.ids.add(value['_id'])
                    continue
                self._collect_ids(value)
            elif isinstance(value, list) and isinstance(value[0], dict) and '_id' in value[0]:
                for item in value:
                    self.ids.add(item['_id'])
            elif isinstance(value, list):
                for item in value:
                    self._collect_ids(item)
            else:
                pass

    def _replace_ids(self, node):
        if not node:
            return node
        for key, value in node.items():
            if not value:
                continue
            elif isinstance(value, dict):
                if '_id' in value:
                    node[key] = self._find_result(value['_id'])
                    continue
                self._replace_ids(value)
            elif isinstance(value, list) and isinstance(value[0], dict) and '_id' in value[0]:
                new_value = []
                for item in value:
                    new_value.append(self._find_result(item['_id']))
                node[key] = new_value
            elif isinstance(value, list):
                for item in value:
                    self._replace_ids(item)
            else:
                pass
        return node

    def _find_result(self, id):
        for item in self.results:
            if item['_id'] == id:
                new_item = item.copy()
                new_item.pop('_id', None)
                return new_item

    def _get_chunks(self, it, chunk_size):
        chunk = []
        for i in it:
            chunk.append(i)
            if len(chunk) >= chunk_size:
                yield chunk
                chunk = []
        if chunk:
            yield chunk