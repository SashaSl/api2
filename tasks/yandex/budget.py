# -*- coding: utf-8 -*-

from celery import group
from celery.exceptions import SoftTimeLimitExceeded

import cfg.celery
import cfg.common
from utils.tasks import BaseTask
from applications.celery import app
from utils.errors import error_to_db
from tasks.core.yandex.budget.services import *
from utils.tasks import check_if_parsed, get_result
from utils.geobase import extract_and_canonize_region
from tasks.core.yandex.budget.parser import YandexBrowser


MAX_RETRIES = cfg.celery.max_retries
RETRY_DELAY = cfg.celery.default_retry_delay
DEFAULT_QUEUE = cfg.celery.CELERY_DEFAULT_QUEUE
DEFAULT_CACHE_TIMEOUT = cfg.common.default_cache_timeout

@app.task(default_retry_delay=RETRY_DELAY, max_retries=MAX_RETRIES, base=BaseTask, name='yandex.budget.parser_budget')
def yandex_budget(items, region, queue=DEFAULT_QUEUE, cache_timeout=0):
    try:
        def _compile_return_value():
            return_value = []
            for req in items_for_return:
                arguments['request'] = req['request']
                return_value.append({'_id': get_result(yandex_budget.db.results, 'budget', arguments, cache_timeout)['_id']})
            return return_value

        items = [{'request': x['request'].strip()} for x in items]
        items_for_return = items
        initial_items = items

        region = extract_and_canonize_region(region)
        arguments = {
            "region": region,
        }
        if cache_timeout > 0:
            items = check_if_parsed(items, yandex_budget.db.results, 'budget', arguments, cache_timeout)

        if not items:
            return _compile_return_value()

        reqs = [x['request'] for x in items]

        reqs_dict = formatted_reqs_dict_maker(reqs)
        reqs_checker = BudgetRequestsChecker(reqs, reqs_dict)
        bad_reqs = reqs_checker.check()
        if bad_reqs:
            bad_reqs_save(bad_reqs, region, yandex_budget.db.results)
            reqs = bad_reqs_remover(reqs, bad_reqs)
            if not reqs:
                return _compile_return_value()
        sliced_requests = slicer(reqs, 100000)

        ### Create tasks
        subtask_template = yandex_budget_chunk.s(
            region,
            reqs_dict
        ).set(queue=queue)

        subtasks = [subtask_template.clone(args=(slicer(partial_request.split(','), 4000),)) for partial_request in sliced_requests]
        group(subtasks)().get()
        cache_timeout = 3600
        while True:
            empty_items = check_if_parsed(initial_items, yandex_budget.db.results, 'budget', arguments, cache_timeout)
            if empty_items:
                sliced_requests = slicer(formatted_reqs_list_maker([x['request'] for x in empty_items]), 100000)
                ### Create tasks
                subtasks = [subtask_template.clone(args=(slicer(partial_request.split(','), 4000),)) for partial_request in sliced_requests]
                group(subtasks)().get()
                initial_items = empty_items
            else:
                break
        return _compile_return_value()

    except SoftTimeLimitExceeded, e:
        error_to_db()
        raise yandex_budget.retry(args=[items, region], kwargs={'queue': queue, 'cache_timeout': 3600}, exc=e)

    except Exception, e:
        error_to_db()
        raise yandex_budget.retry(exc=e)


@app.task(default_retry_delay=RETRY_DELAY, max_retries=MAX_RETRIES, base=BaseTask, name='yandex.budget.chunk')
def yandex_budget_chunk(request, region, reqs_dict):
    try:
        if not request:
            return
        budget_parser = YandexBrowser()
        budget_parser.budget_command(request, region, reqs_dict)
    except BudgetParserFailed, e:
        request = request[e.attrs:]
        error_to_db()
        raise yandex_budget_chunk.retry(args=[request, region, reqs_dict], exc=e)
    except Exception, e:
        error_to_db()
        raise yandex_budget_chunk.retry(args=[request, region, reqs_dict], exc=e)