# -*- coding: utf-8 -*-

import itertools

import cfg.celery
import cfg.common
from applications.celery import app
from utils.errors import error_to_db
from utils.decorators.tor import Tor
from utils.geobase import extract_and_canonize_region
from utils.tasks import save_result, BaseTask, get_result
from tasks.core.yandex.direct_search.parser import YandexBrowser


MAX_RETRIES = cfg.celery.max_retries
RETRY_DELAY = cfg.celery.default_retry_delay
DEFAULT_CACHE_TIMEOUT = cfg.common.default_cache_timeout


@app.task(default_retry_delay=RETRY_DELAY, max_retries=MAX_RETRIES, base=BaseTask, name='yandex.direct_search.chunk')
def yandex_direct_search_chunk(items, region, max_direct_search_pos=None, cache_timeout=DEFAULT_CACHE_TIMEOUT, subresult=None):
    return_value = subresult or []
    _items = []
    __items = []

    try:
        def iter_items(_items):
            for item in _items:
                request = item['request']
                arguments = {
                    "region": region,
                    "request": request,
                    "max_direct_search_pos": max_direct_search_pos,
                }
                yield item, request, arguments

        region = extract_and_canonize_region(region)
        direct_parser = YandexBrowser()

        for item, request, arguments in iter_items(items):
            result_item = get_result(yandex_direct_search_chunk.db.results, 'direct_search', arguments, cache_timeout)
            if result_item:
                return_value.append(result_item)
                yandex_direct_search_chunk.set_progress(float(len(return_value)) / len(items))
                continue
            _items.append(item)

        @Tor(replace_only_none_proxy=False)
        def process_chunk(proxy=None):
            for item, request, arguments in iter_items(_items):
                try:
                    direct_data = direct_parser.direct(request, region, max_direct_search_pos=max_direct_search_pos, proxy=proxy)
                    _result = {
                        "documents": len(direct_data['result']),
                        "direct": [],
                    }

                    for i in xrange(len(direct_data['result'])):
                        _result['direct'].append({
                            'number': i+1,
                            'url': direct_data['result'][i]['link'],
                            'snippet': direct_data['result'][i]['snippet'],
                            'title': direct_data['result'][i]['title'],
                        })

                    _result = save_result(yandex_direct_search_chunk.db.results, 'direct_search', arguments, _result)
                    yandex_direct_search_chunk.set_progress(float(len(return_value)) / (len(items) + len(subresult or [])))
                    return_value.append(_result)
                except:
                    error_to_db()
                    __items.append(item)

        if _items:
            process_chunk()

        if __items:
            class DirectSearchParserFailed(Exception):
                pass
            raise DirectSearchParserFailed('Some directs are not parsed')

        return_value = map(lambda i: {'_id': i['_id']}, return_value)
        return return_value
    except Exception, e:
        error_to_db()
        return_value = map(lambda i: {'_id': i['_id']}, return_value)
        raise yandex_direct_search_chunk.retry(
            exc=e,
            args=[__items or items, region],
            kwargs=dict(
                max_direct_search_pos=max_direct_search_pos,
                cache_timeout=cache_timeout,
                subresult=return_value
            )
        )

@app.task(base=BaseTask, name='yandex.direct_search.merge')
def yandex_direct_search_merge_chunks(chunks):
    try:
        return list(itertools.chain(*chunks))
    except Exception:
        error_to_db()
        raise