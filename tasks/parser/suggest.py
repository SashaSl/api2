# -*- coding: utf-8 -*-

import itertools

import cfg.celery
from applications.celery import app
from utils.errors import error_to_db
from tasks.core.parser.suggest.parser import *
from utils.geobase import extract_and_canonize_region
from utils.tasks import save_result, get_result, BaseTask


@app.task(default_retry_delay=cfg.celery.default_retry_delay, max_retries=cfg.celery.max_retries, base=BaseTask, name='parser.suggest.chunk')
def parser_suggest_chunk(items, engine, region, iterations=1, cache_timeout=3600):
    try:
        region = extract_and_canonize_region(region)
        suggest_class = engine_class_mapping[engine]
        suggest_parser = suggest_class(region=region)

        return_value = []
        for item in items:
            request = item['request']
            prev_request = item.get('prev_request', None)
            arguments = {
                "engine": engine,
                "region": region,
                "request": request,
                "prev_request": prev_request,
                "iterations": iterations
            }

            result_item = get_result(parser_suggest_chunk.db.results, 'suggest', arguments, cache_timeout)
            if result_item:
                return_value.append(result_item)
                parser_suggest_chunk.set_progress(float(len(return_value)) / len(items))
                continue

            fixed_request, result, efficiency = suggest_parser.get_suggest(
                request, prev_request=prev_request, iterations=iterations
            )

            _result = {
                'efficiency': efficiency,
                'suggest': result
            }
            _result = save_result(parser_suggest_chunk.db.results, 'suggest', arguments, _result)
            return_value.append(_result)
            parser_suggest_chunk.set_progress(float(len(return_value)) / len(items))
        return_value = map(lambda i: {'_id': i['_id']}, return_value)
        return return_value
    except Exception, e:
        error_to_db()
        raise parser_suggest_chunk.retry(exc=e)


@app.task(base=BaseTask, name='parser.suggest.merge')
def parser_suggest_merge_chunks(chunks):
    try:
        return list(itertools.chain(*chunks))
    except Exception:
        error_to_db()
        raise