# -*- coding: utf-8 -*-

import json
import config
import urllib
import urllib2
import operator
from collections import defaultdict

from utils.engine import Engine
from utils.geobase import extract_and_canonize_region, engineregionalparams


class BaseSuggest(object):
    def __init__(self, **kwargs):
        for k, v in kwargs.iteritems():
            setattr(self, k, v)
        self.region = extract_and_canonize_region(kwargs.get('region'))
        assert self.region

    def get_request_position_for_prefix(self, request, prefix):
        _pref, anss, power = self.get_suggest(prefix)
        if request in anss:
            return anss.index(request) + 1


class YandexSuggest(BaseSuggest):
    def __init__(self, **kwargs):
        super(YandexSuggest, self).__init__(**kwargs)

        if not hasattr(self, 'server'):
            self.server = 'serp_ru'

        erp = engineregionalparams(self.region, Engine.ENGINE_YANDEX)

        self._server = config.SERVERS[self.server]
        self._region = erp['region_code']
        self._domain = erp['domain']

    def get_suggest(self, prefix, prev_request=None, iterations=1):
        def _true_suggest_get(suggests):
            suggest_dict = defaultdict(int)
            for item in suggests:
                suggest_dict[json.dumps(item)] += 1
            _max = max(suggest_dict.iteritems(), key=operator.itemgetter(1))
            return json.loads(_max[0]), _max[1]

        url = config.SUGGEST_URL['yandex'].format(
            REGION=self._region,
            SERVER=self._server,
            DOMAIN=self._domain,
            PREFIX=urllib.quote_plus(prefix.encode('utf-8'))
        )

        headers = config.HEADERS.copy()

        if prev_request is not None:
            headers['Referer'] = config.PREV_REQUEST_URL['yandex'].format(
                DOMAIN=self._domain,
                REGION=self._region,
                REQUEST=urllib.quote_plus(prev_request.encode('utf-8'))
            )
        suggests = []
        request = urllib2.Request(url, None, headers)
        req = None
        for it in xrange(iterations):
            result = urllib2.urlopen(request).read()
            result = json.loads(result)
            req, anss = result[0], result[1]

            _anss = []
            for ans in anss:
                if isinstance(ans, basestring):
                    _anss.append(ans)
                elif isinstance(ans, list) and len(ans) >= 2:
                    _anss.append(ans[1])
            suggests.append(_anss)
        true_suggest, value = _true_suggest_get(suggests)
        return req, true_suggest, float(value)/iterations


class GoogleSuggest(BaseSuggest):
    def __init__(self, **kwargs):
        super(GoogleSuggest, self).__init__(**kwargs)

        if not hasattr(self, 'hl'):
            self.hl = 'ru'
        if not hasattr(self, 'is_instant'):
            self.is_instant = False
        self._client = 'psy-ab' if self.is_instant else 'heirloom-hp'

        erp = engineregionalparams(self.region, Engine.ENGINE_GOOGLE)

        self._region = erp['region_code']
        self._domain = erp['domain']
        self._hl = erp['params'].get('hl', 'ru')

    def get_suggest(self, prefix, prev_request=None, iterations=1):
        prefix = urllib.quote_plus(prefix.encode('utf-8'))
        url = config.SUGGEST_URL['google'].format(
            DOMAIN=self._domain,
            PREFIX=prefix,
            CLIENT=self._client,
        )

        headers = config.HEADERS.copy()
        headers['Cookie'] = 'PREF=ID=0000000000000000:LD={HL}:L={REGION}'.format(HL=self._hl, REGION=self._region)

        result = urllib2.urlopen(urllib2.Request(url, headers=headers)).read()

        result = config.RESULT_MASK['google'].findall(result)[0]
        result = json.loads(result)

        req = result[0]
        anss = list(map(lambda x: x[0].replace('<b>', '').replace('</b>', ''), result[1]))
        return req, anss, 1.0


class MailSuggest(BaseSuggest):
    def __init__(self, **kwargs):
        super(MailSuggest, self).__init__(**kwargs)

    def get_suggest(self, prefix, prev_request=None, iterations=1):
        prefix = urllib.quote_plus(prefix.encode('utf-8'))
        url = config.SUGGEST_URL['mail'].format(PREFIX=prefix)
        result = urllib2.urlopen(urllib2.Request(url, headers=config.HEADERS)).read()
        _json = json.loads(result)

        result = [item['text'] for item in _json['items']]
        return _json['terms']['query'], result, 1.0


class BingSuggest(BaseSuggest):
    def __init__(self, **kwargs):
        super(BingSuggest, self).__init__(**kwargs)

    def get_suggest(self, prefix, prev_request=None, iterations=1):
        prefix = urllib.quote_plus(prefix.encode('utf-8'))
        url = config.SUGGEST_URL['bing'].format(PREFIX=prefix)
        result = urllib2.urlopen(urllib2.Request(url, headers=config.HEADERS)).read()
        _json = json.loads(result)
        result = [item['Txt'] for item in _json['AS']['Results'][0]['Suggests']]
        return _json['AS']['Query'], result, 1.0


engine_class_mapping = {
    Engine.ENGINE_YANDEX: YandexSuggest,
    Engine.ENGINE_GOOGLE: GoogleSuggest,
    Engine.ENGINE_MAIL: MailSuggest,
    Engine.ENGINE_BING: BingSuggest
}