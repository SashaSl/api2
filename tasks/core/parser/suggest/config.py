# -*- coding: utf-8 -*-

import re

SERVERS = {
    "morda_ru": "morda_ru",
    "serp_ru": "serp_ru"
}

SUGGEST_URL = {
    'yandex': 'http://suggest.yandex.{DOMAIN}/suggest-ya.cgi?v=4&lr={REGION}&srv={SERVER}&part={PREFIX}',
    'google': 'https://www.google.{DOMAIN}/s?gs_ri=heirloom-hp&sclient={CLIENT}&q={PREFIX}',
    'mail': 'http://suggests.go.mail.ru/sg_u?q={PREFIX}',
    'bing': 'http://api.bing.com/qsonhs.aspx?q={PREFIX}'
}

PREV_REQUEST_URL = {
    'yandex': 'http://yandex.{DOMAIN}/yandsearch?lr={REGION}&text={REQUEST}',
    'google': None,
    'mail': None,
    'bing': None
}

RESULT_MASK = {
    'yandex': None,
    'google': re.compile("^window\.google\.ac\.h\((.*)\)$")
}

HEADERS = {
    'User-Agent': 'Mozilla/5.0'
}