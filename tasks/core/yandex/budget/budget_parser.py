#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import json

# ##############
# import os
# os.environ['DISPLAY'] = ':0'
# os.environ['XAUTHORITY'] = '/home/sasha/.Xauthority'
# ######

sys.path.append(sys.argv[4])

from parser import YandexBrowser


def main():
    request = sys.argv[3]
    region = sys.argv[1]
    reqs_dict = sys.argv[2]

    with open(reqs_dict) as f:
        reqs_dict = json.load(f)
    with open(request) as f:
        request = json.load(f)

    ya = YandexBrowser()
    ya.budget_surfbot(request, region, reqs_dict, tor_pid=None)

if __name__ == "__main__":
    main()