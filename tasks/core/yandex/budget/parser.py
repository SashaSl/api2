# -*- coding: utf-8 -*-

import re
import os
import json
import signal
import random
import tempfile
import lxml.html
from subprocess import Popen, PIPE

import cfg.root
import cfg.common
from utils.db.common import DB
from utils.engine import Engine

from surfbot import WebApplication
from utils.tasks import save_result
from utils.common import keys_finder
from utils.decorators.tor import Tor
from utils.errors import error_to_db
from utils.decorators.xvfb import Xvfb
from utils.text.simple import random_string
from utils.geobase import engineregionalparams

from tasks.core.yandex.budget.services import alert_text_parser, SRB_save, BudgetParserFailed


ROOT_DIR = cfg.root.ROOT_DIR
XVFB_VISIBLE = cfg.common.xvfb_visible
ANTI_CAPTCHA_KEY = cfg.common.anti_captcha_key


class YandexBrowser():
    @Tor(replace_only_none_proxy=False)
    @Xvfb(visible=XVFB_VISIBLE, width=1200, height=600, timeout=30)
    def budget_surfbot(self, requests, region, reqs_dict, proxy=None, tor_pid=None):
        try:
            def _handle_result(html, errors_dict):
                if html != 'empty':
                    result = _budget_data_extractor(html)
                    _save_budget_result(result, reqs_dict, DB.db.results)
                if errors_dict:
                    _save_budget_errors(errors_dict, reqs_dict, region, DB.db.results)

            def _additional_budget_html_extractor(request):
                errors_dict = {}
                _clean_phrases()
                _input_new_phrases(request)


                self.page.wait_for(_add_cond, 10)
                _ajax_captcha()

                if not self.page.evaluate('window.fsm')[0]:
                    pass
                else:
                    alert_text = str(self.page.evaluate('window.fsm')[0].toUtf8())
                    errors_dict = alert_text_parser(alert_text)
                    if errors_dict:
                        for k, v in errors_dict.iteritems():
                            request = request.replace(k, '')
                        if not request:
                            return 'empty', errors_dict
                        self.page.evaluate('window.fsm = []')

                        _clean_phrases()
                        _input_new_phrases(request.replace("'", "\\'"))

                        self.page.wait_for(_add_cond, 10)
                        _ajax_captcha()
                return self.page.content, errors_dict

            def _input_new_phrases(request):
                js_script = u"document.getElementById('ad-words').value = '{REQUEST}';".format(REQUEST=request.replace("'", "\\'"))
                self.page.evaluate(js_script)
                self.page.find_elements('input[onclick="add_phrase({new_phrases: true}); return false;"]')[0].click_mouse()

            def _clean_phrases():
                self.page.evaluate('window.confirm = function() {return true;}')
                self.page.evaluate('window.alert = function(msg) {window.fsm = msg;}')
                self.page.find_elements('a[onclick="delete_all_phrases(); return false;"]')[0].click_mouse()

            def _add_cond():
                if self.page.evaluate('window.fsm')[0]:
                    return True
                if self.page.find_elements('tr[class="tdata old-tr-top"][id]'):
                    return True
                if self.page.find_elements('div.i-popup.i-popup_autoclosable_yes.i-popup_fixed_yes.i-bem.i-popup_js_inited.i-popup_visibility_visible img.b-ajax-captcha__img'):
                    return True

            def _cond():
                if self.page.evaluate('window.fsm')[0]:
                    return True
                if self.page.find_elements('tr[class="tdata old-tr-top"][id]'):
                    return True
                if self.page.find_elements('form[action] img[alt="captcha"]'):
                    return True

            def _ajax_captcha():
                counter = 0
                while counter < 3:
                    counter += 1
                    try:
                        self.page.find_elements('div.i-popup.i-popup_autoclosable_yes.i-popup_fixed_yes.i-bem.i-popup_js_inited.i-popup_visibility_visible img.b-ajax-captcha__img')[0]
                        captcha_text = self.page.solve_captcha('div.i-popup.i-popup_autoclosable_yes.i-popup_fixed_yes.i-bem.i-popup_js_inited.i-popup_visibility_visible img.b-ajax-captcha__img')
                        self.page.evaluate('document.querySelector(\'div.i-popup.i-popup_autoclosable_yes.i-popup_fixed_yes.i-bem.i-popup_js_inited.i-popup_visibility_visible input[class="b-form-input__input"][name="captcha"]\').value={VALUE}'.format(VALUE=captcha_text))
                        self.page.sleep(3000)
                        self.page.evaluate('document.querySelector(\'div.i-popup.i-popup_autoclosable_yes.i-popup_fixed_yes.i-bem.i-popup_js_inited.i-popup_visibility_visible input[class="b-form-button__input"][type="submit"]\').click();')
                        self.page.sleep(6000)
                    except IndexError:
                        return

            def _anticaptcha():
                def _ordinary_captcha():
                    try:
                        while True:
                            self.page.find_elements('form[action] img[alt="captcha"]')[0]
                            print 'ordinary captcha'
                            captcha_text = self.page.solve_captcha('form[action] img[alt="captcha"]')
                            self.page.evaluate('document.querySelector(\'input[type="text"]\').value={VALUE}'.format(VALUE=captcha_text))
                            self.page.evaluate('document.querySelector(\'input[type="submit"]\').click();')
                            self.page.wait_for_page_loaded()
                    except IndexError:
                        return

                _ordinary_captcha()

            def _budget_data_extractor(html):
                result = []
                doc = lxml.html.document_fromstring(html)
                doc = doc.cssselect('table[id="result_table"]')[0]
                rows = doc.cssselect('tr')
                for row in rows:
                    row_result = {}
                    req_data = {}
                    if not 'tdata' in row.attrib['class']:
                        continue
                    request = row.cssselect('div.my_div em')[0].text_content()

                    td_labels = row.cssselect('td[nowrap][align] label')
                    for td_label in td_labels:
                        checker = [val for val in ['id', 'for', 'style'] if val in td_label.attrib.keys()]
                        if not checker:
                            displays = td_label.text_content()
                            break

                    req_data['special_placement'] = {
                        'click_cost': float(row.cssselect('td[nowrap][align] label[id^="price_text1"]')[0].text_content()),
                        'CTR': float(row.cssselect('td[nowrap][align] label[id^="ctr_text1"]')[0].text_content()),
                        'displays': float(displays),
                        'budget': float(row.cssselect('td[nowrap][align] span[id^="sum_text1"]')[0].text_content()),
                    }

                    req_data['1st_place'] = {
                        'click_cost': float(row.cssselect('td[nowrap][align] label[id^="price_text2"]')[0].text_content()),
                        'CTR': float(row.cssselect('td[nowrap][align] label[id^="ctr_text2"]')[0].text_content()),
                        'budget': float(row.cssselect('td[nowrap][align] span[id^="sum_text2"]')[0].text_content()),
                    }

                    req_data['guaranteed_display'] = {
                        'click_cost': float(row.cssselect('td[nowrap][align] label[id^="price_text3"]')[0].text_content()),
                        'CTR': float(row.cssselect('td[nowrap][align] label[id^="ctr_text3"]')[0].text_content()),
                        'budget': float(row.cssselect('td[nowrap][align] span[id^="sum_text3"]')[0].text_content()),
                    }

                    row_result['request'] = request
                    row_result['result'] = req_data
                    result.append(row_result)
                return result

            def _budget_html_extractor(request):
                errors_dict = {}
                account = random.choice(list(DB.db.users.find()))
                js_script = "document.getElementById('login').value = '{LOGIN}';".format(LOGIN=account['login'])
                self.page.evaluate(js_script)
                js_script = "document.getElementById('passwd').value = '{PASSWORD}';".format(PASSWORD=account['password'])
                self.page.evaluate(js_script)
                self.page.evaluate('document.forms[0].submit();')
                self.page.wait_for_selector('div[class="b-head-line"]')
                self.page.wait_for_page_loaded()

                try:
                    attention = self.page.find_elements('input[name="continue"]')[0]
                    self.page.evaluate('document.querySelector(\'input[name="continue"]\').click();')
                except IndexError:
                    pass

                _anticaptcha()

                self.page.wait_for_selector('h2[class="hide_this_for_print"]')
                self.page.wait_for_page_loaded()
                js_script = u"document.getElementById('ad-words').value = '{REQUEST}';".format(REQUEST=request.replace("'", "\\'"))
                self.page.evaluate(js_script)
                js_script = "document.getElementById('geo').value = '{REGION}';".format(REGION=engineregionalparams(region, Engine.ENGINE_YANDEX)['region_code'])
                self.page.evaluate(js_script)
                self.page.evaluate('window.alert = function(msg) {window.fsm = msg;}')
                self.page.evaluate('document.querySelector(\'input[id="btn_submit"]\').click();')

                self.page.wait_for(_cond, 10)
                self.page.wait_for_page_loaded()

                if None in self.page.evaluate('window.fsm'):
                    pass
                else:
                    alert_text = str(self.page.evaluate('window.fsm')[0].toUtf8())
                    errors_dict = alert_text_parser(alert_text)
                    if errors_dict:
                        for k, v in errors_dict.iteritems():
                            request = request.replace(k, '')
                        if not request:
                            return 'empty', errors_dict
                        js_script = u"document.getElementById('ad-words').value = '{REQUEST}';".format(REQUEST=request.replace("'","\\'"))
                        self.page.evaluate(js_script)
                        self.page.evaluate('window.fsm = []')
                        self.page.evaluate('document.querySelector(\'input[id="btn_submit"]\').click();')
                        self.page.wait_for(_cond, 10)
                        self.page.wait_for_page_loaded()

                _anticaptcha()
                old_timeout = self.page.wait_timeout
                self.page.wait_timeout = 10
                try:
                    self.page.wait_for_selector('tr[class="tdata old-tr-top"]')
                except Exception:
                    _ajax_captcha()
                    self.page.evaluate('document.querySelector(\'input[id="btn_submit"]\').click();')
                    self.page.wait_timeout = old_timeout
                    self.page.wait_for_selector('tr[class="tdata old-tr-top"]')
                self.page.wait_for_page_loaded()
                return self.page.content, errors_dict

            def _save_budget_result(result, reqs_dict, collection):
                for item in result:
                    for key in keys_finder(reqs_dict, item['request']):
                        arguments = {
                            "region": region,
                            "request": key,
                        }
                        final_result = {
                            "request": key,
                            "budget": item['result'],
                            'formatted_request': item['request']
                        }
                        save_result(collection, 'budget', arguments, final_result)
                        SRB_save(key, region, item['result'], DB.db.budget)

            def _save_budget_errors(errors_dict, reqs_dict, region, collection):
                for k, v in errors_dict.iteritems():
                    for key in keys_finder(reqs_dict, k):
                        arguments = {
                            "region": region,
                            "request": key.encode('utf-8'),
                        }
                        final_result = {
                            "request": key,
                            "budget": 'error',
                            'formattetd_request': k,
                            'error': {'error_type': 'yandex error',
                                      'error_value': v}
                        }
                        save_result(collection, 'budget', arguments, final_result)

            cache_dir = os.path.join(tempfile.gettempdir(), 'webkit-network-cache-' + random_string())
            if not os.path.exists(cache_dir):
                os.makedirs(cache_dir)
            self.app = WebApplication(
                open_links_in_new_tabs=False,
                wait_for_page_load_when_open=True,
                proxy=proxy,
                antigate_key=ANTI_CAPTCHA_KEY,
                viewport_size=(1200, 600),
                user_agent='Mozilla/5.0',
                cache_dir=cache_dir,
                homedir=os.path.join(tempfile.gettempdir(), 'webkit-home-dir-' + random_string())
            )
            url = 'https://direct.yandex.ru/registered/main.pl?cmd=ForecastByWords'
            self.app.load_url_in_tab(url, True)
            self.page = self.app.get_current_web_page()

            first = True
            for request in requests:
                if first:
                    html, errors_dict = _budget_html_extractor(request)
                    _handle_result(html, errors_dict)
                    first = False
                else:
                    html, errors_dict = _additional_budget_html_extractor(request)
                    _handle_result(html, errors_dict)
                print 'success_item'
            print 'abbastanza'
            if tor_pid:
                os.kill(tor_pid, signal.SIGKILL)
        except Exception:
            error_to_db()
            raise

    def budget_command(self, request, region, reqs_dict):
        with tempfile.NamedTemporaryFile() as reqs_dict_file, tempfile.NamedTemporaryFile() as request_file:
            json.dump(reqs_dict, reqs_dict_file, indent=4)
            reqs_dict_file.flush()
            json.dump(request, request_file)
            request_file.flush()
            cmd = ". venv/bin/activate && python {ROOT}/tasks/core/yandex/budget/budget_parser.py {REGION} {REQS_DICT} {REQUEST} {ROOT}".format(
                ROOT=ROOT_DIR,
                REQUEST=request_file.name,
                REGION=region,
                REQS_DICT=reqs_dict_file.name,
            )
            out, err = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE).communicate()
        if not 'abbastanza' in out:
            success_items = len(list(re.finditer('success_item', out)))
            raise BudgetParserFailed('Failed to parse budget!', success_items)