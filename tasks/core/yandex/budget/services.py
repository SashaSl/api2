# -*- coding: utf-8 -*-

import re
import datetime
from collections import defaultdict

from utils.tasks import save_result
from utils.common import keys_finder
from utils.text.simple import punct_remover
from utils.text.lingvistic import CachingMorphAnalyzer


def SRB_save(request, region, data, collection):
    item = {
        'search_request': request,
        'region': region,
        'shows': data['special_placement']['displays'],

        'clicks_premium': data['special_placement']['CTR']*data['special_placement']['displays']/100,
        'clicks_firstplace': data['1st_place']['CTR']*data['special_placement']['displays']/100,
        'clicks_guaranteed': data['guaranteed_display']['CTR']*data['special_placement']['displays']/100,

        'price_premium': data['special_placement']['click_cost'],
        'price_firstplace': data['1st_place']['click_cost'],
        'price_guaranteed': data['guaranteed_display']['click_cost'],

        'ctr_premium': data['special_placement']['CTR'],
        'ctr_firstplace': data['1st_place']['CTR'],
        'ctr_guaranteed': data['guaranteed_display']['CTR'],

        'created': datetime.datetime.now()
    }
    collection.insert(item)


def sanitize_request4budget(request):
    request = request.replace('/', ' ').replace('_', '').replace(' ! ', ' ')
    if '-' in request and not ('- ' in request or ' -' in request or ' - ' in request):
        request = request.replace('-', ' ')
    request = request.replace('+', '')
    request = ' '.join(request.split())
    request = re.sub('\+$', '', request)
    return request


def formatted_reqs_dict_maker(reqs):
    reqs_dict = {}
    for req in reqs:
        reqs_dict[req] = sanitize_request4budget(req)
    return reqs_dict


class HashableDict(dict):
    __hash__ = lambda self: hash(str(self))
    pass


class BudgetRequestsChecker:
    def __init__(self, reqs, reqs_dict):
        self.reqs = reqs
        self.reqs_dict = reqs_dict
        self.bad_reqs = defaultdict(set)
        self.morph = CachingMorphAnalyzer()

    def _symbols_checker(self):
        alphabet_checker = AlphabetChecker()
        for item in self.reqs:
            result = alphabet_checker.check(item, 'budget_parser')
            if result[0] is False:
                for key in keys_finder(self.reqs_dict, item):
                    self.bad_reqs[key].add(HashableDict(error_type='unsupported symbols', error_value=list(result[1])))

    def _length_checker(self):
        for item in self.reqs:
            if item.startswith('"') and item.endswith('"'):
                length = len(punct_remover(item))
                if length > 7:
                    for key in keys_finder(self.reqs_dict, item):
                        self.bad_reqs[key].add(HashableDict(error_type='too long', error_value=length))
            else:
                item_list = punct_remover(item)
                for word in item_list[:]:
                    morph_word = self.morph.parse(word)
                    if morph_word:
                        if morph_word[0].tag.POS in ['PREP', 'CONJ', 'PRCL']:
                            item_list.remove(word)
                if len(item_list) > 7:
                    for key in keys_finder(self.reqs_dict, item):
                        self.bad_reqs[key].add(HashableDict(error_type='too long', error_value=len(item_list)))

    def _punct_checker(self):
        for item in self.reqs:
            for simbols in [' -', '- ', ' - ']:
                if simbols in item:
                    for key in keys_finder(self.reqs_dict, item):
                        self.bad_reqs[key].add(HashableDict(error_type='unsupported symbols', error_value=simbols))

    def _set_to_list(self, dct):
        new = {}
        for k, v in dct.iteritems():
            new[k] = list(v)
        return new

    def check(self):
        self._symbols_checker()
        self._length_checker()
        self._punct_checker()
        return self._set_to_list(self.bad_reqs)


class AlphabetChecker:
    def __init__(self):
        alphabet = {
            'en': 'abcdefghigklmnopqrstuvwxyz',
            'ru': u'абвгдеёжзийклмнопрстуфхцчшщъыьэюя',
            'ua': u'абвгґдеєжзиіїйклмнопрстуфхцчшщьюя',
            'kz': u'аәбвгғдеёжзийкқлмнңоөпрстуұүфхһцчшщъыіьэюя',
            'tr': u'abcçdefgğhıijklmnoöprsştuüvyz',
            'decimal': '1234567890',
            'symbols': ' "-+!,.',
        }
        self.alphabet_sets = {}
        for lang, alph in alphabet.iteritems():
            self.alphabet_sets[lang] = set(alph)
        budget_parser_langs = ['en', 'ru', 'ua', 'kz', 'tr', 'decimal', 'symbols']
        bp_alphabet_str = ''
        for lang in budget_parser_langs:
            bp_alphabet_str += alphabet[lang]
        self.alphabet_sets['budget_parser'] = set(bp_alphabet_str)

    def check(self, string, lang):
        string = string.lower()
        if lang in self.alphabet_sets:
            diff = set(string) - self.alphabet_sets[lang]
            if not diff:
                return True, False
            else:
                return False, diff


def bad_reqs_save(bad_reqs, region, collection):
    for k, v in bad_reqs.iteritems():
        arguments = {
            "region": region,
            "request": k,
        }

        final_result = {
            "request": k,
            "budget": 'error',
            "error": v,
            'formatted_request': sanitize_request4budget(k)
        }
        save_result(collection, 'budget', arguments, final_result)


def bad_reqs_remover(reqs, bad_reqs):
    for k in bad_reqs.iterkeys():
        reqs.remove(sanitize_request4budget(k))
    return reqs


def slicer(reqs, length):
    sliced_requests = []
    new_request = ''
    while reqs:
        if not len(new_request) + len(reqs[0]) + 1 > length:
            new_request += reqs[0] + ','
            reqs = reqs[1:]
        else:
            sliced_requests.append(new_request[:len(new_request)-1])
            new_request = ''
    if len(new_request):
        sliced_requests.append(new_request[:len(new_request)-1])
    return sliced_requests


def formatted_reqs_list_maker(reqs):
    formatted_reqs = []
    for req in reqs:
        formatted_reqs.append(sanitize_request4budget(req))
    return formatted_reqs


def alert_text_parser(alert_text):
    errors_dict = {}
    alert_text = alert_text.replace('\n', '')
    for item in alert_text.split(';'):
        if item.endswith(')'):
            pattern = re.compile('\((.*?)\)')
            request = re.findall(pattern, item)[-1] if re.findall(pattern, item) else False
            if request:
                error = item.replace('(' + request + ')', '').strip()
                errors_dict[request] = error
        if item.endswith('"'):
            pattern = re.compile('\"(.*?)\"')
            request = re.findall(pattern, item)[-1] if re.findall(pattern, item) else False
            if request:
                error = item.replace('"' + request + '"', '').strip()
                errors_dict[request] = error
        if item.endswith('""'):
            pattern = re.compile('\""(.*?)\""')
            request = re.findall(pattern, item)[-1] if re.findall(pattern, item) else False
            if request:
                error = item.replace('""' + request + '""', '').strip()
                errors_dict['"' + request + '"'] = error
    if errors_dict:
        errors_dict = dict(map(lambda (x, y): (x.decode('utf8'), y.decode('utf8')), errors_dict.iteritems()))

    return errors_dict


class BudgetParserFailed(Exception):
    def __init__(self, title, attrs):
        self.title = title
        self.attrs = attrs
        super(BudgetParserFailed, self).__init__()
