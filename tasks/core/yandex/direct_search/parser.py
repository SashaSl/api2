# -*- coding: utf-8 -*-

import lxml.html
from lxml import etree
from urllib import quote_plus

import cfg.common
from utils.engine import Engine
from utils.browser import Browser
from utils.geobase import engineregionalparams
from utils.anticaptcha import recognize_captcha_url
from utils.decorators.connection_timeout import connection_timeout

TMT = cfg.common.browser_max_timeout


class YandexBrowser(Browser):
    @connection_timeout(TMT)
    def direct(self, request, region, max_direct_search_pos=None, proxy=None):
        if proxy is not None:
            self.set_proxy(proxy)

        if max_direct_search_pos is None:
            max_page = 1000
        else:
            max_page = max_direct_search_pos/20 + 1 if max_direct_search_pos % 20 != 0 else max_direct_search_pos/20

        direct = []
        page = 0
        request = request.encode("utf-8")
        erp = engineregionalparams(region, Engine.ENGINE_YANDEX)

        while page < max_page:
            page += 1
            url = 'http://direct.yandex.{DOMAIN}/search?text={REQUEST}&rid={REGION}&page={PAGE}'.format(
                REGION=erp['region_code'],
                DOMAIN=erp['domain'],
                REQUEST=quote_plus(request),
                PAGE=page,
            )
            br, page_result = self._direct_page(url)
            if br:
                break
            direct = direct + page_result
        if max_direct_search_pos is not None: direct = direct[:max_direct_search_pos]
        return {
            'result': direct,
            'user_profile': self.user_profile()
        }

    def _direct_page(self, url):
        page_result = []
        browser = self.browser
        browser.open(url, timeout=TMT)
        browser._factory.is_html = True

        html = browser.response().read()

        while True:
            tree = etree.HTML(html)
            captcha = tree.xpath("//img[@alt='captcha']")
            if captcha:
                print "captcha!"
                img_url = captcha[0].attrib['src']
                text = recognize_captcha_url(img_url, True)
                browser.select_form(nr=1)
                browser.form['captcha_code'] = text
                browser.submit()
            else:
                break
            browser._factory.is_html = True
            html = browser.response().read()

        if 'yandex.st/lego' not in html:
            class PageIsNotYandex(Exception):
                pass
            raise PageIsNotYandex('Page is not Yandex!')

        doc = lxml.html.document_fromstring(html.decode('utf-8'))
        block = doc.cssselect('.b-map-page__banner-list li')

        if not block:
            br = True
            return br, []
        else:
            br = False

        for item in block:
            element = {}

            try:
                ad_title = item.cssselect('.ann .banner-selection .ad .ad-link a')[0].text_content()
            except IndexError:
                ad_title = ''

            try:
                ad_snippet = item.cssselect('.ann .banner-selection .ad div')[1].text_content()
            except IndexError:
                ad_snippet = ''

            try:
                ad_link = item.cssselect('.ann .banner-selection .ad .url .domain')[0].text_content()
            except IndexError:
                ad_link = ''

            element['title'] = ad_title
            element['snippet'] = ad_snippet
            element['link'] = ad_link

            warning = item.cssselect('.ann .banner-selection .ad .url span.b-adv-alert__text')
            if warning:
                element['warning'] = warning[0].text_content()

            page_result.append(element)
        return br, page_result