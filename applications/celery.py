# -*- coding: utf-8 -*-

from __future__ import absolute_import

import cfg.celery
from celery import Celery
from celery.signals import task_revoked


@task_revoked.connect
def _task_revoked(*args, **kwargs):
    """
    При отмене таски - отменяем все таски из данной группы.
    """
    task_id = kwargs['request'].id
    from utils.tasks import cancel_task
    cancel_task(task_id)


app = Celery()
app.config_from_object(cfg.celery)