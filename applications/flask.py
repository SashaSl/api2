# -*- coding: utf-8 -*-

from __future__ import absolute_import

import cfg.flask
from flask import Flask
from utils.json_encoder import ApiJSONEncoder

app = Flask(__name__)
app.config.from_object(cfg.flask)

# Устанавливаем кастомный json-кодировщик для корректной сериализации
# объектов с типами datetime.datetime и ObjectId
app.json_encoder = ApiJSONEncoder
