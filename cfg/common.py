# -*- coding: utf-8 -*-

### xvfb ###
xvfb_visible = False

### tor ###
tor_on = True

### cache ###
default_cache_timeout = 604800

### serp ###
max_serp_pos = 50
serp_chunk_size = 25

### wait_for_tasks ###
wait_for_tasks_timeout = 1

### browser ###
browser_max_timeout = 90

### captcha ###
anti_captcha_key = '#########'
anti_captcha_timeout = 5
anti_captcha_api_url = '#########'
anti_captcha_boundary = '#########'
anti_captcha_domain = '#########'

### suggest ###
suggest_chunk_size = 100

### direct ###
direct_search_chunk_size = 25

### wordstat ###
wordstat_maxpage = 1
wordstat_chunk_size = 100