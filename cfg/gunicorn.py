# -*- coding: utf-8 -*-

import os
from installation_type import installation_type
from .root import ROOT_DIR

import multiprocessing

if installation_type in {'local', 'dev'}:
    api_port = 8080
else:
    api_port = 443

gunicorn_workers = multiprocessing.cpu_count() * 2 + 1
gunicorn_max_requsts = 1000
gunicorn_timeout = 60
gunicorn_graceful_timeout = 60
gunicorn_keepalive = 2
gunicorn_limit_request_line = 0
gunicorn_limit_request_fields = 32768
gunicorn_limit_request_field_size = 0
gunicorn_debug = False
gunicorn_chdir = ROOT_DIR
gunicorn_pythonpath = ROOT_DIR
gunicorn_loglevel = 'INFO'
gunicorn_access_log_format = '%(h)s %(l)s %(u)s %(t)s %(L)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"'
gunicorn_proc_name = 'api_gunicorn'
gunicorn_default_proc_name = 'api_gunicorn'
gunicorn_certfile = os.path.join(ROOT_DIR, 'install/openssl/server.crt')
gunicorn_keyfile = os.path.join(ROOT_DIR, 'install/openssl/server.key')
