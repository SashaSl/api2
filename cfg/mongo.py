# -*- coding: utf-8 -*-

from installation_type import installation_type

if installation_type in {'local', 'dev'}:
    mongodb_host = '127.0.0.1'
    mongodb_password = '#########'
else:
    mongodb_host = '92.63.110.72'
    mongodb_password = '#########'

mongodb_dbname = 'api'
mongodb_user = 'api'
mongodb_url = 'mongodb://{user}:{password}@{host}/{db}'.format(user=mongodb_user, password=mongodb_password, host=mongodb_host, db=mongodb_dbname)