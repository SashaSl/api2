# -*- coding: utf-8 -*-

from installation_type import installation_type

if installation_type in {'local', 'dev'}:
    redis_host = '127.0.0.1'
    redis_password = '#########'
else:
    redis_host = '#########'
    redis_password = '#########'