# -*- coding: utf-8 -*-

from kombu import Queue, Exchange
from .mongo import *
from .redis import *
from .rabbitmq import *

from installation_type import installation_type

queues_names = ['default', 'high', 'low']

if installation_type in {'local', 'dev'}:
    ADMINS = []
    workers_info = [
        {'name': 'default', 'concurrency': 4, 'queues': ['default', 'high']},
        {'name': 'low', 'concurrency': 4, 'queues': ['default', 'high', 'low']},
        {'name': 'high', 'concurrency': 4, 'queues': ['high']}
    ]
else:
    ADMINS = [
        [u'Давид Курякин', 'dkuryakin@evristic.ru'],
        [u'Покемон Покемонович', 'pokemon@evristic.ru']
    ]
    workers_info = [
        {'name': 'default', 'concurrency': 48, 'queues': ['default', 'high']},
        {'name': 'low', 'concurrency': 48, 'queues': ['default', 'high', 'low']},
        {'name': 'high', 'concurrency': 48, 'queues': ['high']}
    ]

celery_workers = ' '.join([item['name'] for item in workers_info])

celery_multi_options = ' '.join(
    ['-c:{} {}'.format(item['name'], item['concurrency']) for item in workers_info] +
    ['-Q:{} {}'.format(item['name'], ','.join(item['queues'])) for item in workers_info]
)

CELERY_IMPORTS = ['tasks']
BROKER_URL = rabbitmq_url
# BROKER_URL = 'redis://:{password}@{host}/0'.format(password=redis_password, host=redis_host)
# BROKER_TRANSPORT_OPTIONS = {
#     'visibility_timeout': 3600
# }

CELERY_RESULT_BACKEND = mongodb_url
CELERY_MONGODB_BACKEND_SETTINGS = {
    'host': mongodb_host,
    'user': mongodb_user,
    'password': mongodb_password,
    'database': mongodb_dbname,
    'taskmeta_collection': 'taskmeta_collection',
}

CELERY_TASK_SERIALIZER = 'pickle'
CELERY_RESULT_SERIALIZER = 'pickle'
CELERY_IGNORE_RESULT = False
CELERY_ACCEPT_CONTENT = ['pickle']
CELERY_TIMEZONE = 'Europe/Moscow'
CELERY_ENABLE_UTC = True

CELERYD_PREFETCH_MULTIPLIER = 1
CELERY_RESULT_PERSISTENT = True

CELERY_ACKS_LATE = True
CELERY_DISABLE_RATE_LIMITS = True

CELERY_CREATE_MISSING_QUEUES = False
CELERY_DEFAULT_QUEUE = workers_info[0]['name']
CELERY_QUEUES = [Queue(item, Exchange(item), item) for item in queues_names]

CELERY_TASK_RESULT_EXPIRES = 60 * 60 * 24 * 2
CELERY_TRACK_STARTED = True
CELERYD_POOL_RESTARTS = True

CELERYD_TASK_TIME_LIMIT = 2 * 3600 * 24
CELERYD_TASK_SOFT_TIME_LIMIT = 3600 * 24

CELERY_SEND_TASK_ERROR_EMAILS = True

SERVER_EMAIL = '#########'
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_HOST_USER = '#########'
EMAIL_HOST_PASSWORD = '#########'
EMAIL_PORT = 587
EMAIL_USE_SSL = False
EMAIL_USE_TLS = False

CELERY_SEND_EVENTS = True
CELERY_SEND_TASK_SENT_EVENT = True

default_retry_delay = 30
max_retries = 30

### ALARM: Following settings NOT WORK AT ALL!!! Don't even try to use them!
# CELERYD_FORCE_EXECV = True
# CELERYD_MAX_TASKS_PER_CHILD = 1