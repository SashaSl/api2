# -*- coding: utf-8 -*-

from installation_type import installation_type

if installation_type in {'local', 'dev'}:
    rabbitmq_username = 'admin'
    rabbitmq_password = '#########'
    rabbitmq_host = '127.0.0.1'
else:
    rabbitmq_username = 'admin'
    rabbitmq_password = '#########'
    rabbitmq_host = '#########'

rabbitmq_url = 'amqp://{user}:{password}@{host}:5672//'.format(user=rabbitmq_username, password=rabbitmq_password, host=rabbitmq_host)