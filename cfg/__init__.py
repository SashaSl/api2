# -*- coding: utf-8 -*-

import sys

try:
    from installation_type import installation_type
except ImportError:
    _error_msg = 'Некорректный файл installation_type.py! Учтите:\n'\
                 '    1) В корне проекта должен лежать файл installation_type.py.\n'\
                 '    2) В этом файле должна быть определена переменная installation_type.'
    print >> sys.stderr, _error_msg
    raise

# Здесь я умышленно не делаю что-нибудь типа
# >>> from . import celery
# т.к. не все из субмодулей функционируют до полной установки.