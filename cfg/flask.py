# -*- coding: utf-8 -*-

from installation_type import installation_type

if installation_type in {'local', 'dev'}:
    DEBUG = True
    TESTING = True
    api_http_user = '#########'
    api_http_password = '#########'
    SECRET_KEY = '#########'
else:
    DEBUG = False
    TESTING = False
    api_http_user = '#########'
    api_http_password = '#########'
    SECRET_KEY = '#########'
