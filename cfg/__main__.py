# -*- coding: utf-8 -*-

import os
import sys

ROOT_DIR = os.path.dirname(__file__)
ROOT_DIR = os.path.abspath(os.path.join(ROOT_DIR, '..'))
sys.path.append(ROOT_DIR)

module, variable = sys.argv[1].split('.')
module = __import__('cfg.' + module, globals(), locals(), [variable], 0)
variable = getattr(module, variable)
sys.stdout.write(str(variable))
sys.stdout.flush()