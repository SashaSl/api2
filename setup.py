# -*- coding: utf-8 -*-

"""
e-api2
------

Парсеры и инструменты (глубокий бэкенд =).
"""

from setuptools import setup

setup(
    name='e-api2',
    packages=['api_interface'],
    install_requires=['requests']
)